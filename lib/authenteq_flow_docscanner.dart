
import 'dart:async';
import 'package:flutter/services.dart';

class AuthenteqFlowDocscanner {
  static const MethodChannel _channel = MethodChannel('authenteq_flow_docscanner');

  /// Obtain Authenteq SDK version
  static Future<String> get getPlatformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

}
