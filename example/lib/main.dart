import 'package:flutter/material.dart';
import 'dart:async';
import 'package:authenteq_flow/authenteq_flow.dart';
import 'package:authenteq_flow/models/IdentificationParameters.dart';
import 'package:authenteq_flow/models/IdentificationResult.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _version = 'Unknown';
  dynamic _identificationResult;
  Exception? _exception;

  @override
  void initState() {
    super.initState();
    loadSdkVersion();
  }

  Future<void> loadSdkVersion() async {
    String version;
    try {
      version = await AuthenteqFlow.getVersion;
    } on Exception {
      version = 'Failed to get SDK version.';
    }

    if (!mounted) return;

    setState(() {
      _version = version;
    });
  }

  Future<void> startIdentification() async {
    IdentificationResult? result;
    Exception? exception;
    try {
      IdentificationParameters parameters = IdentificationParameters();
      parameters.clientId = '< MY CLIENT ID >';
      parameters.clientSecret = '< MY SECRET KEY >';
      parameters.theme = {
        'primaryColor': '#00a2ff',
        'AndroidStyle': 'AuthenteqCustom',
        'identificationInstructionImageForDriverLicense': 'graphics/driver.png'
      };
      result = await AuthenteqFlow.identification(parameters);
    } on Exception catch(e) {
      exception = e;
    }

    setState(() {
      _identificationResult = result;
      _exception = exception;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Authenteq Flow Plugin example'),
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Spacer(),
              ElevatedButton(
                onPressed: () {
                  startIdentification();
                },
                child: const Text('Start Identification'),
              ),
              Text('Running with Authenteq SDK: $_version\n'),
              const Spacer(),
              exceptionWidget(),
              resultWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Widget exceptionWidget() {
    return _exception == null ? Container() : Text(_exception.toString(), style: const TextStyle(color: Colors.red));
  }

  Widget resultWidget() {
    return _identificationResult == null ? Container() : Text(
        "Verification ID: ${_identificationResult.verificationId}"
    );
  }
}
