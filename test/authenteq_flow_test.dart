import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:authenteq_flow_docscanner/authenteq_flow_docscanner.dart';

void main() {
  const MethodChannel channel = MethodChannel('authenteq_flow_docscanner');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await AuthenteqFlowDocScanner.getPlatformVersion, '42');
  });
}
