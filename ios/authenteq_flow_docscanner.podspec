Pod::Spec.new do |s|
  s.name             = 'authenteq_flow_docscanner'
  s.version          = '1.75.0'
  s.summary          = 'A new flutter plugin project.'
  s.description      = <<-DESC
A new flutter plugin project.
                       DESC
  s.homepage         = 'http://authenteq.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Authenteq' => 'support@authenteq.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.dependency 'AuthenteqFlowDocScanner', '1.75.0'
  s.platform = :ios, '11.0'

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  s.swift_version = '5.0'
end
